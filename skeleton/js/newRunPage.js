// Prefix to use for Local Storage.  You may change this.
// var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];
var _distance = 0;
var curLocMarker
//var currentLocation = []; //[0]:lat [1]:lon [2]:accuracy

// GPS from sensor app    


// Displaying Map
mapboxgl.accessToken = 'pk.eyJ1IjoidGVycmFuY2UwMCIsImEiOiJjazg4N3hiNGcwMG80M21teno1OXZseWxyIn0.9wSAxC4E13m9js90fUkBZg'

var position 
var current_Lat = []
var current_Lon = []
var current_Acc = []
var exact_Lat = []
var exact_Lon = []
var marker = null
var currentLocation = [];
//var markers = [];
var markerList = [];
var runDistanceLeft;
var map 
var _destination
function getCurrentLocation(position)
    {
        // get the current latitude and longitude:
        //currentLocation = [Number(position.coords.latitude).toFixed(4),Number(position.coords.longitude).toFixed(4),Number(position.coords.accuracy).toFixed(2)];
        
        //curLoc
        current_Lon = Number(position.coords.longitude.toFixed(4));
        current_Lat = Number(position.coords.latitude.toFixed(4));
        current_Acc = Number(position.coords.accuracy.toFixed(2));
        current_Loc = [current_Lon,current_Lat,current_Acc]
        console.log(current_Loc)

        // map = new mapboxgl.Map({
        // container: 'map', // container id
        // style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        // center: [current_Lon,current_Lat], // starting position [lng, lat]
        // zoom: 16 // starting zoom
        // });
        if(typeof map!=="undefined"){
        
        if(curLocMarker){
            curLocMarker.remove();
        }
        var curLocMarker = new mapboxgl.LngLat(current_Lon,current_Lat)
        map.panTo(curLocMarker)
        var curLocMarker = new mapboxgl.Marker({color:"green"}).setLngLat(curLocMarker).addTo(map);

        var curLocMarker = new mapboxgl.Popup({offset: 45}).setText("Current Position").addTo(map);
    }
        if(typeof _destination!=="undefined"){
            _destination = _destination;
            var track = [current_Lon,current_Lat]
            track.push(_destination)
            runDistanceLeft = distance(track)
        }
        
        if(typeof runDistanceLeft!=="undefined"){
            if(runDistanceLeft<=10){
                //end run
                newRun._endTime = _date.getTime();
                newRun._duration = newRun._endTime - newRun._startTime; //(ms)
            }
        }

        var curLocMarker = new mapboxgl.Popup({offset: 45}).setText("Current Position").addTo(map)
        
        var distanceLeft = distance(current_Lat,current_Lon,newLat,newLong)
        if (distanceLeft <= 10) {
        }
    }

navigator.geolocation.watchPosition(getCurrentLocation)

map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [current_Lon,current_Lat], // starting position [lng, lat]
        zoom: 16 // starting zoom
        });



/*
function trackLocation() {
setInterval(function() {
    navigator.geolocation.watchPosition(getCurrentLocation)
},1000)
}
*/


modeButtonClicked()

//function to generate new destinaiton
function allowButtonPresses()
{
    buttonsAllowed = true;
}

function disallowButtonPresses()
{
    buttonsAllowed = false;
}

function modeButtonClicked() {
    if(currentLocation[2]<20){
    modeIcon.textContent = "Button is Enabled";
    allowButtonPresses()
    randomDestination();
    } else {
    disallowButtonPresses()
    alert("Accuracy of gps is not accurate enough");
    }   
}
var markerLnglat
var marker1
var newLong = 0;
var newLat = 0;

 function randomDestination(e) {
    allowButtonPresses()
    _distance = 0

    while(_distance<60){
        var r = 150/111300 //Radius in Degree
        let initialLon = current_Lon
        let initialLat = current_Lat
        let a = Math.random()
        let b = Math.random()
        let w = r * Math.sqrt(a)
        let t = 2 * Math.PI * (b)
        let x = w * Math.cos(t)
        let changeLon = x / Math.cos(initialLat*(Math.PI / 180))
        let changeLat = w * Math.sin(t)
        newLong = initialLon+changeLon
        newLat = initialLat+changeLat
        _distance = distance(initialLat,initialLon,newLat,newLong);
        //console.log(_distance)
    }
     
    markerLnglat = new mapboxgl.LngLat(newLong,newLat);
    map.panTo(markerLnglat)
     if(marker1){
        marker1.remove()
     }
    marker1 = new mapboxgl.Marker()
    .setLngLat(markerLnglat)
    .addTo(map);
    }

 function showPath()
        {
            // Code added here will run when the "Show Path" button is clicked.
map.addSource('route', {
    'type': 'geojson',
    'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates':[
                [current_Lon,current_Lat], 
                [newLong,newLat]
       ]
     }
  }
});
                
map.addLayer({
    'id': 'route',
    'type': 'line',
    'source': 'route',
    'layout': {
        'line-join': 'round',
        'line-cap': 'round'
    },
    'paint': {
        'line-color': '#888',
        'line-width': 8
    }
});
        }



var actualRunName;
var insideTable = ""
function submitClick(){
    
    actualRunName = document.getElementById("runNameInput").value
    localStorage.setItem("recentSaveName",actualRunName)
    //console.log(actualRunName);
    console.log("Run Name: " + actualRunName)
    newRun = new run(actualRunName,[current_Lon,current_Lat],[newLong,newLat],_date.getTime(),_distance)
    let estimatedDistance =_distance;
    document.getElementById("estDistance").innerHTML = `Estimated Distance : ${estimatedDistance.toFixed(2)} m`;

}

function beginRun(){
    let addRunNameInput = document.getElementById('runName');
    let NameInput = "";
    NameInput += "<form>Enter run name:<input type='text' id='runNameInput'><input type='button' value='Begin run!' onclick='submitClick();showPath()'></form>"
    addRunNameInput.innerHTML=NameInput;
}



//Feature 6
var STORAGE_KEY = "runNameInput"
var localArray = []
var localRunNames = []

console.log(insideTable)

//moved SavedAction() to shared.js


/*function disableSave() {
    document.getElementById('saveAction').disabled = true;
}
document.getElementById("saveAction").addEventListener("click",disableSave)
*/
function saveRunName() {
    let saveNameInput = ""
    let addSaveNameInput = document.getElementById("saveName")
    saveNameInput += "<form>Enter Save name:<input type='text' id='saveRunNameInput'><input type='button' value='Save' onclick='saveAction();'></form>"
    addSaveNameInput.innerHTML = saveNameInput
}

 
function retrieveRun()
{
    if (typeof(Storage) !== "undefined")
    {
        // TODO: Retrieve the stored JSON string and parse
        //       to a variable called runObject.
        //       Use this to initialise an new instance of
        //       the Run class.
        
        var runObject = JSON.parse(localStorage.getItem("x")) 
        runInstance = new Run();
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }

}

/*
//Feature 7
function displayRetrievedRun()
{
    let outputAreaRef = document.getElementById("outputArea");
    let output = "";

    if (runInstance)
    {
        output += runInstance;
    }
    else
    {
        output = "Error: Run not loaded.<br />"
    }

    outputAreaRef.innerHTML = output;
}
*/