// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
var selectedRun

function viewRun(index)
{
    //get list of save Names of the runs
    var runArray = JSON.parse(localStorage.getItem("runName"));
    //get the selected Run 'SaveName' that stored in LocalStorage...
    selectedRun = runArray[index]
    
    console.log(selectedRun+" save name selected");
    
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem("selectedRun",selectedRun);
    // ... and load the View Run page.
    location.href = 'viewRun.html';

    
}




    
