// Shared code needed by all three pages.
mapboxgl.accessToken = 'pk.eyJ1IjoiYmVhbm1vbW8iLCJhIjoiY2s4emYwbWE0MDV0ZjNrb2w5d25pZ2ZreSJ9.iEQ26u_uXzAhuOPnV56y_Q';
 
var _date = new Date();


//Added run class
class run
{
    constructor(name,startPosition, endPosition, startTime, distance)
    {
        this._runName = name
        this._startPosition = startPosition;
        this._endPosition = endPosition;
        this._startTime = startTime;
        this._distance = distance;
        this._dateCreated = new Date();
        
    }
    getRunName(){
        return this._runName
    }
    
    getStartPosition() {
        return this._startPosition
    }
    setStartPosition(newStartPosition){
        this._startPosition = newStartPosition
    }
    getEndPosition() {
        return this._endPosition
    }
    setEndPosition(newEndPosition){
        this._endPosition = newEndPosition
    }
    getStartTime() {
        return this._startTime
    }

    

}
//Update The main page Run List
function refresh(){

    var SavedTable = localStorage.getItem("insideTableSaved")
    
    document.getElementById("runsList").innerHTML = SavedTable
    


}
//array that used for storing all saved 'Save name' of the run
var runName = []
//start from -1 cause why not(see line 76 and think)
var runIndex = -1

//Where the magic happens
function saveAction()
{   
    //what the user put recorded in this variable
    actualSaveName = document.getElementById("saveRunNameInput").value

    if(localStorage.getItem(actualSaveName) !== null){
        alert("Save name already exist, it will overwrite the on saved on this name... too baddd")
    }
    //checls if runName and index local storage name exist or not
    if(localStorage.getItem("runName") !== null){
        runName = JSON.parse(localStorage.getItem("runName"))
    }
    if(localStorage.getItem("index") !== null){
        runIndex = localStorage.getItem("index")
    }
    
    //runIndex used to update the index on onClick for the saved run list on mainpage
    runIndex++
    localStorage.setItem("index",runIndex)
    //Update runName array
    runName.push(actualSaveName);
    localStorage.setItem("runName",JSON.stringify(runName))

    if (typeof(Storage) !== "undefined")
    {
    
    JSON.stringify(newRun)
        localStorage.setItem(actualSaveName,JSON.stringify(newRun))
        console.log(newRun)
    }   else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }

    //check if insideTable in local storage exist or not otherwise it will overwrite not add....
    if (localStorage.getItem("insideTableSaved") !== null){
    insideTable = localStorage.getItem("insideTableSaved");
    }
    
    
    //add a new line for main page consist of RunName and Savename
    insideTable += "<li class='mdl-list__item mdl-list__item--two-line' onclick='viewRun("+runIndex+")'><span class='mdl-list__item-primary-content'><span id='"+actualSaveName+"'>"+actualRunName+"</span><span class='mdl-list__item-sub-title'>On Save Named : "+actualSaveName+"</span></span></li>"
    localStorage.setItem('insideTableSaved', insideTable);
    console.log("insideTable Updated");
    
    
}

//distance function source: https://stackoverflow.com/questions/31192451/generate-random-geo-coordinates-within-specific-radius-from-seed-point/31280435
function distance(lon1, lat1, lon2, lat2) {
    var R = 6371000;
    var a = 0.5 - Math.cos((lat2 - lat1) * Math.PI / 180) / 2 + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * (1 - Math.cos((lon2 - lon1) * Math.PI / 180)) / 2;
    return R * 2 * Math.asin(Math.sqrt(a));
}