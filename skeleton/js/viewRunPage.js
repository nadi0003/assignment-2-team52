// Code for the View Run page.
var current_Acc; 
var current_Lat; 
var current_Lon;



// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var runNameThatWillBeViewed = localStorage.getItem("selectedRun");
console.log(runNameThatWillBeViewed+" selected");
var selectedRunObject = JSON.parse(localStorage.getItem(runNameThatWillBeViewed))
// If a run index was specified, show name in header bar title. This
// is just to demonstrate navigation.  You should set the page header bar
// title to an appropriate description of the run being displayed.
    
document.getElementById("headerBarTitle").textContent = selectedRunObject._runName;
var stats = document.getElementById("detail");
let insideStats = "";

insideStats += "<div>Distance : "+(selectedRunObject._distance).toFixed(2);

if(selectedRunObject._endTime){
    var runDuration = (selectedRunObject._endtime - selectedRunObject._startTime)/1000; //in (s)
    insideStats += "<br>Duration : "+runDuration;

}else{
    insideStats += "<br>Duration : <i>run is not yet completed</i></div>";
}

insideStats += "<br><button id='startRun' onclick='startRun()'>Re-attempt run</button>"

stats.innerHTML = insideStats;
//show map ,start ,end marker

var startLoc = selectedRunObject._startPosition;

var viewMap = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
    center: startLoc, // starting position [lng, lat]
    zoom: 16 // starting zoom
    });


var startMarker = new mapboxgl.LngLat(startLoc[0],startLoc[1]);
viewMap.panTo(startMarker);
var startMarker = new mapboxgl.Marker({color:"black"}).setLngLat(startMarker).addTo(viewMap);


var endLoc = selectedRunObject._endPosition
//console.log(endLoc)
var endMarker = new mapboxgl.LngLat(endLoc[0],endLoc[1]);
var endMarker = new mapboxgl.Marker().setLngLat(endMarker).addTo(viewMap);




function startRun(){
    alert("Any existing data such as Duration, path will be replaced")
    
    //add path from point to point
    viewMap.addSource('route', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates':[
                    [startLoc[0],startLoc[1]], 
                    [endLoc[0],endLoc[1]]
           ]
         }
      }
    });
                    
    viewMap.addLayer({
        'id': 'route',
        'type': 'line',
        'source': 'route',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': '#888',
            'line-width': 8
        }
    });

    selectedRunObject._startTime = _date.getTime();
    navigator.geolocation.watchPosition(getCurrentLocation);


    //navigator.blablabla
}
    
function getCurrentLocation(position)
    {
        // get the current latitude and longitude:
        //currentLocation = [Number(position.coords.latitude).toFixed(4),Number(position.coords.longitude).toFixed(4),Number(position.coords.accuracy).toFixed(2)];
        
        //curLoc
        current_Lon = Number(position.coords.longitude.toFixed(4));
        current_Lat = Number(position.coords.latitude.toFixed(4));
        current_Acc = Number(position.coords.accuracy.toFixed(2));
        current_Loc = [current_Lon,current_Lat,current_Acc]
        console.log(current_Loc)

        // map = new mapboxgl.Map({
        // container: 'map', // container id
        // style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        // center: [current_Lon,current_Lat], // starting position [lng, lat]
        // zoom: 16 // starting zoom
        // });
        if(typeof viewMap!=="undefined"){
        
        if(curLocMarker){
            curLocMarker.remove();
        }
        var curLocMarker = new mapboxgl.LngLat(current_Lon,current_Lat)
        viewMap.panTo(curLocMarker)
        var curLocMarker = new mapboxgl.Marker({color:"green"}).setLngLat(curLocMarker).addTo(viewMap);

        var curLocMarker = new mapboxgl.Popup({offset: 45}).setText("Current Position").addTo(viewMap);
    }
        if(typeof _destination!=="undefined"){
            _destination = _destination;
            var track = [current_Lon,current_Lat]
            track.push(_destination)
            runDistanceLeft = distance(current_Lon,current_Lat,endLoc[0],endLoc[1])
        }
        
        if(typeof runDistanceLeft!=="undefined"){
            if(runDistanceLeft<=10){
                //end run
                selectedRunObject._endTime = _date.getTime();
                selectedRunObject._duration = selectedRunObject._endTime - selectedRunObject._startTime; //(ms)
            }
        }

        var curLocMarker = new mapboxgl.Popup({offset: 45}).setText("Current Position").addTo(viewMap)
        
        var distanceLeft = distance(current_Lon,current_Lat,endLoc[0],endLoc[1])
        if (distanceLeft <= 10) {
        }
    }